#include "HAF_header.h"

void file_decomp_ini(FILE *file_in,FILE *file_out)
{
	int sign;
	int end_offset = 0;
	char buffer_ini[300] = {0};
	int i,count_mas = 0;
	unsigned long long last_sym;
	int tail;

	NODE_POINT root = NULL;
	NODE_POINT massive_sign[258] = {NULL};

	fread(&buffer_ini,sizeof(char),2,file_in);

	if(!(buffer_ini[0] == 'V' && buffer_ini[1] == 'N'))
	{
		printf("file is not correct a version is not VN");
		system("PAUSE");
		exit(5);
	}
	
		fread(&end_offset,sizeof(int),1,file_in);
		for(i = 0;i <= end_offset;i++)
		{
			massive_sign[count_mas] = (NODE_POINT)malloc(sizeof(SYM));
			*massive_sign[count_mas]->ciper = '\0';
			massive_sign[count_mas]->left_node = NULL;
			massive_sign[count_mas]->flag = 1;
			massive_sign[count_mas]->right_node = NULL;
			fread(&massive_sign[count_mas]->sign,sizeof(char),1,file_in);
			fread(&massive_sign[count_mas]->count,sizeof(int),1,file_in);
			count_mas++;
		
		}
	fread(&tail,sizeof(int),1,file_in);

	root = building_haffman(massive_sign);
	create_ciper(root,"");
	decoder(root,(long long int)tail,file_in);
}

NODE_POINT seek_sign(NODE_POINT root,unsigned char sign)
{
	NODE_POINT temp;
	if(!root)
		return NULL;

	if(root->sign == sign && root->flag == 1) 
			return root;
		if(root->left_node) 
		{
			temp = seek_sign(root->left_node,sign);
			if(temp==NULL)
			   return seek_sign(root->right_node,sign);
		}
		else
			return seek_sign(root->right_node,sign);

}

void decoder(NODE_POINT root,long long int teil,FILE *file)
{

	int index_bit = 0;
	int i;
	unsigned int sign = 0;
	unsigned int bit_mask = 0;
	int offset = 7;
	long long int percent = teil;
	int per_befor = 0;
	NODE_POINT point_seek = root;


	fread(&bit_mask,sizeof(char),1,file);
//	fread(&bit_mask,sizeof(int),1,file);


	
	
	while(1)
	{
		if(offset < 0)
		{
			offset = 7;
			fread(&bit_mask,sizeof(char),1,file);
		}

	if(!feof(file))
	{
		if(bit_mask & (1<<offset))
		{
			point_seek = point_seek->right_node;
			offset--;
		}
		else {point_seek = point_seek->left_node; offset--;}

		if(point_seek->flag == 1)
		{
			fwrite((unsigned char*)&point_seek->sign,1,1,file_decoder);
			point_seek = root;
			index_bit++;
			if(index_bit == teil-1) break;

			percent --;

		}
		if(per_befor != 100 - (percent * 100) / teil)
		{
		printf("\r%ld%c",per_befor = (100-((percent * 100) / teil)),'%');
		//per_befor = (percent * 100) / teil;
		}
	}
	else break;

	}
}




void fill_fast_mas(NODE_POINT root, fast_node *mass)
{
	if(root)
	{
		fill_fast_mas(root->left_node,mass);
		fill_fast_mas(root->right_node,mass);
		if(root->flag == 1)
		{
			mass[root->sign].sign = root->sign;
			mass[root->sign].offset = 32;
			for(int i = 0;i < strlen(root->ciper);i++)
			{
				if(root->ciper[i] == '1')
				{
					mass[root->sign].bit_mask |= 1 << --mass[root->sign].offset;
					//mass[root->sign].offset--;
				}
				else mass[root->sign].offset--;
			}
		}
	}

}


int create_haffman_output(NODE_POINT root, FILE *file,int signs_before)
{
#define SIZE_BUFFER 10000
	unsigned int bit_mask = 0;
	unsigned char sign[SIZE_BUFFER] = {0};
	int i,i1=0,i2 = 0;
	short offset = 0;
	bool flag = 0;
	int control = 101;
	long long count = 0;
	long long percent = 0;
	fast_node fast_massive[257] = {0};
	unsigned char buffer[1000] = {'\0'};

	rewind(file);

	fill_fast_mas(root,fast_massive);

	fwrite(&signs_before,sizeof(int),1,file_exit);

	/////////////unlink
	while(1)
	{
		fread(&sign,sizeof(char),SIZE_BUFFER,file);
		percent += SIZE_BUFFER;

		if(!feof(file))
		{
			for(i2 = 0;i2 < SIZE_BUFFER;i2++)
			{

		
				bit_mask |= (fast_massive[sign[i2]].bit_mask) >> offset;
				offset += 32-(fast_massive[sign[i2]].offset);

				//fwrite((unsigned char*)&bit_mask,sizeof(char),1,file_exit);

				while(offset >= 8)
				{
					buffer[i1++] = bit_mask >> 24;
					count++;
					bit_mask = bit_mask << 8;
					offset -= 8;
				}

				

				if(i1 > 900)
				{
					fwrite(buffer,sizeof(char),i1,file_exit);

					i1 = 0;
				}
			}

		if((((percent * 100)/signs_before)) != control) flag = 1;
		if(flag == 1) 
		{
		//	system("CLS");
			printf("\r%2.2f\%",(float)((percent * 100)/(signs_before))); flag = 0;
			control =(((percent * 100)/(signs_before)));
		}
	}
	else if(feof(file) && signs_before % SIZE_BUFFER)
	{
		for(i2 = 0;i2 < signs_before % SIZE_BUFFER;i2++)
		{

		
				bit_mask |= (fast_massive[sign[i2]].bit_mask) >> offset;
				offset += 32 - (fast_massive[sign[i2]].offset);
				//fwrite((unsigned char*)&bit_mask,sizeof(char),1,file_exit);
				while(offset >= 8)
				{
					buffer[i1++] = bit_mask >> 24;
					bit_mask = bit_mask << 8;
					offset -= 8;
				}

				count++;

				if(i1 > 900)
				{
					fwrite(buffer,sizeof(char),i1,file_exit);
					i1 = 0;
				}
			}

		if((((percent * 100)/signs_before)) != control) flag = 1;
		if(flag == 1) 
		{
		//	system("CLS");
			printf("\r%2.2f\%",(float)((percent * 100)/(signs_before))); flag = 0;
			control =(((percent * 100)/(signs_before)));
		}
		break;
	}
	}
	buffer[i1++] = bit_mask >> 24;
	fwrite(buffer,sizeof(char),i1,file_exit);;

	system("CLS");
	printf("It is done!\nPercent of compress:%2.2f",(float)(100 - ((count * 100)/percent)));
	return 0;
}


void print_haf(NODE_POINT root)
{
	if(root)
	{
		print_haf(root->left_node);
		printf("%d ",root->count);
		print_haf(root->right_node);
	}
}

void create_ciper(NODE_POINT root,char *sign)
{
	if(root)
	{
		strcat(root->ciper,sign);
		if(root->left_node)strcpy(root->left_node->ciper,root->ciper);
		if(root->right_node)strcpy(root->right_node->ciper,root->ciper);
		create_ciper(root->left_node,"0");
		create_ciper(root->right_node,"1");
	}
}



NODE_POINT building_haffman(NODE_POINT *massive)
{
	int size_elm = strlen_SYM(massive);
	while(size_elm >= 1)
	{
		massive[0] = create_haffman_node(massive[0],massive[1]);
		del_elm_mas(massive,1);
		sort_massive(massive,--size_elm);
	}
	return massive[0];
}

NODE_POINT create_haffman_node(NODE_POINT left,NODE_POINT right)
{
	if(left && right)
	{
	NODE_POINT new_node = (NODE_POINT)malloc(sizeof(sign_node));
	new_node->count = (left->count + right->count);
	new_node->sign = NULL;
	new_node->flag = 0;
	memset(new_node->ciper,'\0',64);
	new_node->left_node = left;
	new_node->right_node = right;
	return new_node;
	}
	else {printf("error haf"); exit(2);}
}


int strlen_SYM(NODE_POINT *massive)
{
	int count = 0;
	while(massive[count] && count <= 256)
		count++;
	return count-1;
}

void del_elm_mas(NODE_POINT *massive,int elm)
{
	int i;
	for( i = elm ; i < 256;i++)
	{
		massive[i] = massive[i+1];
	}
	massive[i-1] = NULL;
}


void sort_massive(NODE_POINT *massive,int size_mas)
{
	int i,i1,flag = 1;
	NODE_POINT temp;

	for(i = 0;i < size_mas-1;i++)
	{
		if(massive[i]->count > massive[i+1]->count)
		{
			temp = massive[i];
			massive[i] = massive[i+1];
			massive[i+1] = temp;
		}
	}
}


void prepear_exit_file(NODE_POINT *massive)
{
	int i, mas_end = strlen_SYM(massive);

	fwrite("VN",sizeof(char),2,file_exit);
	fwrite(&mas_end,sizeof(int),1,file_exit);

	for(i = 0;massive[i];i++)
	{
		fwrite(&massive[i]->sign,sizeof(char),1,file_exit);
		fwrite(&massive[i]->count,sizeof(int),1,file_exit);
	}

}


void filling_massive(NODE_POINT *massive_to_filling,NODE_POINT root_of_tree)
{
	if(root_of_tree)
	{
		filling_massive(massive_to_filling,root_of_tree->left_node);
		massive_to_filling[count_regulator] = (NODE_POINT)malloc(sizeof(sign_node));
		massive_to_filling[count_regulator]->count = root_of_tree->count;
		massive_to_filling[count_regulator]->sign = root_of_tree->sign;
		massive_to_filling[count_regulator]->flag = 1;
		massive_to_filling[count_regulator]->left_node = NULL;
		massive_to_filling[count_regulator++]->right_node = NULL;
		filling_massive(massive_to_filling,root_of_tree->right_node);
	}
	//count_regulator = 0;
}


int create_signs_tree(NODE_POINT *root,FILE *file)
{
	int sign;
	int count_byte = 0;
	if(file)
	{
		while(!feof(file))
		{
			fread(&sign,sizeof(unsigned char),1,file);
			if(!feof(file))
			create_node_tree(root,(unsigned char)sign);
			count_byte++;
		}
	}
	else
	{
		printf("error riding file");
		exit(1);
	}
	return count_byte;
}


void create_node_tree(NODE_POINT *root,unsigned char sign)
{
	if(*root == NULL)
	{
		*root = (NODE_POINT)malloc(sizeof(sign_node));
		(*root)->left_node = NULL;
		(*root)->right_node = NULL;
		(*root)->sign = sign;
		(*root)->count = 1;
		(*root)->flag = 1;
		memset((*root)->ciper,'\0',63);
	}
	else if((*root)->sign > sign)
		create_node_tree(&(*root)->left_node,sign);
	else if((*root)->sign < sign)
		create_node_tree(&(*root)->right_node,sign);
	else (*root)->count++;
}


void create_counts_tree(NODE_POINT *root,unsigned char sign,int count,NODE_POINT left,NODE_POINT right)
{
		if(*root == NULL)
	{
		*root = (NODE_POINT)malloc(sizeof(sign_node));
		(*root)->left_node = left;
		(*root)->right_node = right;
		(*root)->sign = sign;
		(*root)->flag = 1;
		(*root)->count = count;
		memset((*root)->ciper,'\0',63);
	}
	else if((*root)->count >= count)
		create_counts_tree(&(*root)->left_node,sign,count,left,right);
	else if((*root)->count < count)
		create_counts_tree(&(*root)->right_node,sign,count,left,right);
	
}


void rebild_tree(NODE_POINT *root,NODE_POINT *new_root)
{
	if(*root)
	{
		rebild_tree(&(*root)->left_node,new_root);
		rebild_tree(&(*root)->right_node,new_root);
		create_counts_tree(new_root,(*root)->sign,(*root)->count,NULL,NULL);
		free(*root);
	}
}


NODE_POINT destribution(NODE_POINT *root)
{
	NODE_POINT new_root = NULL;

	rebild_tree(root,&new_root);

	return new_root;
}


void nulling_tree(NODE_POINT *root)
{
	if(*root)
	{
		nulling_tree(&(*root)->left_node);
		nulling_tree(&(*root)->right_node);
		free(*root);

	}
}

int get_counts(NODE_POINT massive[258],FILE *file_from)

{
	int sign;
	int count_sign = 0;
	int signs = 0;
	int mass_counts[256] = {0};
	NODE_POINT temp = NULL;

	if(!file_from){ printf("error"); exit(1);}

	while(1)
	{
		fread(&sign,sizeof(char),1,file_from);
		
		if(!feof(file_from))
		{
			count_sign++;
			mass_counts[(unsigned char)sign]++;
		}
		else break;
	}
	
	for(int i = 0;i < 256;i++)
	{
		if(mass_counts[i])
		{
			massive[signs] = (NODE_POINT)malloc(sizeof(sign_node));
			massive[signs]->ciper[0] = '\0';
			massive[signs]->count = mass_counts[i];
			massive[signs]->flag = 1;
			massive[signs]->left_node = NULL;
			massive[signs]->right_node = NULL;
			massive[signs]->sign = (unsigned char)i;
			signs++;
		}

	}


//while(!feof(file_from))
//{
//	fread(&sign,sizeof(char),1,file_from);
//	count_sign++;
//
//	if(massive[(unsigned char)sign] != NULL)
//	{
//		massive[(unsigned char)sign]->count++;
//	}
//	else
//	{
//		massive[(unsigned char)sign] = (NODE_POINT)malloc(sizeof(sign_node));
//		massive[(unsigned char)sign]->ciper[0] = '\0';
//		massive[(unsigned char)sign]->count = 1;
//		massive[(unsigned char)sign]->flag = 1;
//		massive[(unsigned char)sign]->left_node = NULL;
//		massive[(unsigned char)sign]->right_node = NULL;
//		massive[(unsigned char)sign]->sign = (unsigned char)sign;
//		signs++;
//	}
//}


//for(int i = 0;i < 256;i++)
//{
//	if(massive[i] == NULL)
//	{
//		for(int j = i; j < 256;j++)
//		{
//			if(massive[j] != NULL)
//			{
//				temp = massive[i];
//				massive[i] = massive[j];
//				massive[j] = temp;
//			}
//
//		}
//
//	}
//
//}


for(int i = 0; i < signs;i++)
{
	if(massive[i] == NULL) break;
	for(int j = 0; j < signs;j++)
	{
		if(massive[j] == NULL) break;
		if(massive[i]->count < massive[j]->count)
		{
			temp = massive[i];
			massive[i] = massive[j];
			massive[j] = temp;
		}

	}

}


return count_sign;
}
