/*
��:
1- ������� ����� ��� ��������� � ����������� ������ ���� ��� ����. ���������� �������� ��� ����
2- ������� ���� ���������� ������� open-gl
3- ���������� ����. 
4- ������� ����� ������� ���������� � ��� ��������� ����������� ����� ������� � ����� ������� ����� ���� �����
5- ������� ��������� ����, ���������� �����������, � �������� ����������� ��� ���������� ���� ��� ������� ������.-
�� ���� ������� ������ �� ����� ��������� ��������� ����������������� � ������ ����.
6- ��������� ��������. ������� ������ �� �������. ������� ���� �������� �����. ��������
7- �������� ����� ���� � ������� ��������� ������ ���� ����. ���� �������� ���� ���������(�������� �������)
*/
#include<glut.h>
#include"field.h"
#include "figures.h"
#include <iostream>

GLint HEIGHT = 800;
GLint WIDTH = 800;

Field field(WIDTH,HEIGHT);
Figures figur(WIDTH,HEIGHT);
void Display();
void Reshape(GLint w_width,GLint w_height);
void timer(int);
void Keyboard(unsigned char,int,int);

int main()
{


	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(WIDTH,HEIGHT);
	glutCreateWindow("[TETRIS](TEST)");
	glMatrixMode(GL_PROJECTION);
	glClearColor(0,0,0,1);
	glLoadIdentity();

	gluOrtho2D(0,WIDTH,HEIGHT,0);
	glutDisplayFunc(Display);
	glutTimerFunc(figur.get_FPS(),timer,0);
	glutReshapeFunc(Reshape);
	glutKeyboardFunc(Keyboard);

	glutMainLoop();
	return 0;
}
void Display()
{
	static int timer = 0;
	timer += figur.get_FPS();
	glClear(GL_COLOR_BUFFER_BIT);

	field.draw();
//	field.draw_test_field();
	figur.draw_game();
	if(timer > figur.get_time_step())
	{
	figur.step_game();
	timer = 0;
	}
	glFinish();
	
}
void timer(int)
{
	Display();
	glutTimerFunc(figur.get_FPS(),timer,0);
}


void Reshape(GLint w_width,GLint w_height)
{
	WIDTH = w_width;
	HEIGHT = w_height;
	glViewport(0,0,w_width,w_height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0,w_width,w_height,0,-1.0,1.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}
void Keyboard(unsigned char key,int x,int y)
{
	std::cout<<"[report]::[KEY IS]<<"<<(int)key<<"[MOUSE COORD]"<<"X="<<x<<"Y="<<y<<std::endl;
	switch(key)
	{
	case'a': case 244: //  ������� � ���������� ���������
		figur.move_left();
		break;
	case'd': case 226:
		figur.move_right();
		break;
	case'w': case 246:
		figur.turn();
		break;
	case 's': case 251:
		figur.step_game();
		break;
	}

}
