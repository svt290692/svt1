#include "field.h"
#include "figures.h"
#include <glut.h>
#include <time.h>

Figures::Figures(GLint size_width,GLint size_height)
	{
		 full_size_height = size_height; // ������ ������ ���� ��� �������� ��������������� � ������
	 	 full_size_width = size_width;
		 size_qube = (((full_size_height)/100) * 4 ); // ������ ������ ������
		 game_field_width = size_qube*10;// ������� �������� ���� ����������� �� ������� ������� ������ 10 ����� ������� 20 �����
		 game_field_height = size_qube*20 ;
		 fill_mass_coords(mass_coord_qubes);
	     SPACE = 0;
		 ACTIVE_FIGURE = 1;
		 PASSIVE_FIGURE = 2;
		 FPS = 1000/60;
		 create_figure();
		 time_per_step = 1000;
	}

int Figures::create_figure()
{
	int rand_choise=0;
	int report = 0;
	int count_coord = 0;
	static const bool qube_provision[4][10] = {
	{0,0,0,0,1,1,0,0,0,0},
	{0,0,0,0,1,1,0,0,0,0},
	{0,0,0,0,0,0,0,0,0,0},
	{0,0,0,0,0,0,0,0,0,0}};

	static const bool stick_provision[4][10] = {
	{0,0,0,0,0,1,0,0,0,0},
	{0,0,0,0,0,1,0,0,0,0},
	{0,0,0,0,0,1,0,0,0,0},
	{0,0,0,0,0,1,0,0,0,0}};

	static const bool g_shape_provision[4][10] = {
	{0,0,0,0,0,1,0,0,0,0},
	{0,0,0,0,0,1,0,0,0,0},
	{0,0,0,0,1,1,0,0,0,0},
	{0,0,0,0,0,0,0,0,0,0}};

	static const bool z_shape_provision[4][10] = {
	{0,0,0,0,1,0,0,0,0,0},
	{0,0,0,0,1,1,0,0,0,0},
	{0,0,0,0,0,1,0,0,0,0},
	{0,0,0,0,0,0,0,0,0,0}};

	srand((unsigned)time(NULL));

	rand_choise = rand() % 4;
	turn_shape = 0;

	switch(rand_choise)
	{
	case stick:
		for(int i = 0; i < 4;i++)
		for(int j = 0; j < 10;j++)
		{
			if(data_mas_fig[i][j] != 0) {report = 1;break;}
			else if(stick_provision[i][j] == true)
			{
				data_mas_fig[i][j] = 1;
				coords_current_shape[count_coord].X = j;
				coords_current_shape[count_coord++].Y = i;
			}
		}
		break;
	case qube:
		for(int i = 0; i < 4;i++)
		for(int j = 0; j < 10;j++)
		{
			if(data_mas_fig[i][j] != 0) {report = 1;break;}
			else if(qube_provision[i][j] == true)
			{
				data_mas_fig[i][j] = 1;
				coords_current_shape[count_coord].X = j;
				coords_current_shape[count_coord++].Y = i;
			}
		}
		break;
	case z_shape:
		for(int i = 0; i < 4;i++)
		for(int j = 0; j < 10;j++)
		{
			if(data_mas_fig[i][j] != 0) {report = 1;break;}
			else if(z_shape_provision[i][j] == true)
			{
				data_mas_fig[i][j] = 1;
				coords_current_shape[count_coord].X = j;
				coords_current_shape[count_coord++].Y = i;
			}
		}
		break;
	case g_shape:
		for(int i = 0; i < 4;i++)
		for(int j = 0; j < 10;j++)
		{
			if(data_mas_fig[i][j] != 0) {report = 1;break;}
			else if(g_shape_provision[i][j] == true)
			{
				data_mas_fig[i][j] = 1;
				coords_current_shape[count_coord].X = j;
				coords_current_shape[count_coord++].Y = i;
			}
		}
		break;
	}


	if(report != 1){ activ_fig = true; current_shape = rand_choise;}
	return report;
}

void Figures::draw_figur(COORD *coord)
{
	srand((unsigned)time(NULL));
	glColor3f(0,0,1);
	glBegin(GL_QUADS);
	glVertex2i(coord->X,coord->Y);
	glVertex2i(coord->X,coord->Y+size_qube);
	glVertex2i(coord->X+size_qube,coord->Y+size_qube);
	glVertex2i(coord->X+size_qube,coord->Y);
	glEnd();
}

void Figures::draw_game()
{
	for(int i = 0;i < 20;i++)
	{
		for(int j = 0;j < 10;j++)
		{
			if(data_mas_fig[i][j] != 0 ) draw_figur(&mass_coord_qubes[i][j]);
		}

	}

}
void Figures::step_game()
{
	bool report = true;
	for(int i = 0; i < 4;i++)
	{
		if(data_mas_fig[coords_current_shape[i].Y+1][coords_current_shape[i].X] == PASSIVE_FIGURE || coords_current_shape[i].Y == 19) report = false;
	}
	if(report)
	{
		move_down();
	}
	else
	{
		unactive_fig();

		if(create_figure())
		{
			exit(1);
		}
	}

	del_line(check_line()); // ��� ������������� �������� �� ������������� ����� ��������� ��� �����

}
void Figures::unactive_fig()
{
	for(int i = 0; i < 4;i++)
	{
		data_mas_fig[coords_current_shape[i].Y][coords_current_shape[i].X] = PASSIVE_FIGURE;
	}

}

int Figures::check_act_fig()
{
	for(int i = 0;i < 20;i++)
	{
		for(int j = 0 ;j < 10;j++)
		{
			if(data_mas_fig[i][j] == 1) return 1;
		}

	}
	return 0;
}


int Figures::check_line()
{
	int check = 1;

	for(int i = 0;i < 20;i++)
	{
		check = 1;
		for(int j = 0;j < 10;j++)
		{
			if(data_mas_fig[i][j] != PASSIVE_FIGURE) check = 0;
		}
		if(check){return i;}
	}
	return 0;
}

void Figures::del_line(int line)
{
	int i = 0;
	if(line)
	{
		for(i = 0; i < 10;i++)
		{
			data_mas_fig[line][i] = SPACE;
		}
		for(i = line-1;i >=0;i--)
		{
			for(short j = 0;j < 10;j++)
			{
				if(data_mas_fig[i][j] == PASSIVE_FIGURE)
				{
					data_mas_fig[i+1][j] = PASSIVE_FIGURE;
					data_mas_fig[i][j] = SPACE;
				}

			}

		}

	}

}

void Figures::move_left()
{
	bool check_position = true;
	int i;
	for(i = 0; i < 4;i++)
	{
		if(coords_current_shape[i].X-1 < 0  || data_mas_fig[coords_current_shape[i].Y][coords_current_shape[i].X-1] == PASSIVE_FIGURE) check_position = false;

	}

	if(check_position)
	{
	for( i = 0 ;i < 4;i++)
	{
		data_mas_fig[coords_current_shape[i].Y][coords_current_shape[i].X] = SPACE;
		coords_current_shape[i].X--;
	}

	for( i = 0 ;i < 4;i++)
	{
		data_mas_fig[coords_current_shape[i].Y][coords_current_shape[i].X] = ACTIVE_FIGURE;
	}
	}
}

void Figures::move_right()
{
	bool check_position = true;
	int i;
	for(i = 0; i < 4;i++)
	{
		if(coords_current_shape[i].X+1 > 9 || data_mas_fig[coords_current_shape[i].Y][coords_current_shape[i].X+1] == PASSIVE_FIGURE) check_position = false;
	}

	if(check_position)
	{
	for( i = 0 ;i < 4;i++)
	{
		data_mas_fig[coords_current_shape[i].Y][coords_current_shape[i].X] = SPACE;
		coords_current_shape[i].X++;
	}

	for( i = 0 ;i < 4;i++)
	{
		data_mas_fig[coords_current_shape[i].Y][coords_current_shape[i].X] = ACTIVE_FIGURE;
	}
	}
}
void Figures::move_down()
{
	
	for(int i = 0 ;i < 4;i++)
	{
		data_mas_fig[coords_current_shape[i].Y][coords_current_shape[i].X] = SPACE;
		coords_current_shape[i].Y++;
	}

	for(int i = 0 ;i < 4;i++)
	{
		data_mas_fig[coords_current_shape[i].Y][coords_current_shape[i].X] = ACTIVE_FIGURE;
	}

}
void Figures::turn()
{
	bool broke = false; // ��������� ��� ������ �� �������� �����) goto �� ���� ������������
	switch(current_shape)
	{
	case qube: break;
	case stick:
		// ������� �������� ����� ��� �������
			for(int i = 0,offset = turn_shape == 0?2:-2;i < 4;i++,turn_shape == 0?offset--:offset++)
			{
				if(data_mas_fig[ coords_current_shape[i].Y + offset][coords_current_shape[i].X + offset] == PASSIVE_FIGURE){broke = true; break;} // �������� �� ����� ��� ��������
				if((coords_current_shape[i].X + offset) > 9 || (coords_current_shape[i].X + offset) < 0){ broke = true;break;}
			}
			if(broke) break;

			del_fig(); // �������� ���������� ������������ ��� ��������� ��������� ������
			for(int i = 0,offset = turn_shape == 0?2:-2;i < 4;i++,turn_shape == 0?offset--:offset++)
			{
				coords_current_shape[i].X += offset;
				coords_current_shape[i].Y += offset;
			}
			restore_fig(); // ������������� ������ �� ����� �����������

			turn_shape == 0 ? turn_shape++ : turn_shape--;
			break;
	case g_shape: // � ��� ��� ������ ������ �� ������ ������ ��� ����� � ������ �������� � ������ ����

		if(turn_shape == 0)
		{
			// ��������
				if(data_mas_fig[coords_current_shape[0].Y+1][coords_current_shape[0].X+1] == PASSIVE_FIGURE
					|| data_mas_fig[coords_current_shape[3].Y-1][coords_current_shape[3].X-1] == PASSIVE_FIGURE
					|| data_mas_fig[coords_current_shape[2].Y+2][coords_current_shape[2].X] == PASSIVE_FIGURE
					|| (coords_current_shape[0].X+1 > 9 || coords_current_shape[2].X < 0)){broke = true; break;}			
			// ����� �������� 
				
		}
		else if(turn_shape == 1)
		{
			// ��������
				if(data_mas_fig[coords_current_shape[0].Y+1][coords_current_shape[0].X-1] == PASSIVE_FIGURE
					|| data_mas_fig[coords_current_shape[3].Y-1][coords_current_shape[3].X+1] == PASSIVE_FIGURE
					|| data_mas_fig[coords_current_shape[2].Y][coords_current_shape[2].X+2] == PASSIVE_FIGURE
					|| (coords_current_shape[0].X-1 < 0 || coords_current_shape[2].X+2 > 9)){broke = true; break;}
			// ����� �������� 
		}
		else if(turn_shape == 2)
		{
			// ��������
				if(data_mas_fig[coords_current_shape[0].Y-1][coords_current_shape[0].X-1] == PASSIVE_FIGURE
					|| data_mas_fig[coords_current_shape[3].Y+1][coords_current_shape[3].X+1] == PASSIVE_FIGURE
					|| data_mas_fig[coords_current_shape[2].Y+2][coords_current_shape[2].X] == PASSIVE_FIGURE
					|| (coords_current_shape[0].X-1 < 0 || coords_current_shape[2].X+1 > 9)){broke = true; break;}
			// ����� �������� 
		}
		else if(turn_shape == 3)
		{
			// ��������
				if(data_mas_fig[coords_current_shape[0].Y-1][coords_current_shape[0].X+1] == PASSIVE_FIGURE
					|| data_mas_fig[coords_current_shape[3].Y+1][coords_current_shape[3].X-1] == PASSIVE_FIGURE
					|| data_mas_fig[coords_current_shape[2].Y][coords_current_shape[2].X-2] == PASSIVE_FIGURE
					|| (coords_current_shape[0].X+1 > 9 || coords_current_shape[2].X-2 < 0)){broke = true; break;}
			// ����� �������� 
		}
		if(broke) break;

		del_fig();

		if(turn_shape == 0)
		{
			coords_current_shape[0].X++;
			coords_current_shape[0].Y++;
			coords_current_shape[3].X--;
			coords_current_shape[3].Y--;
			coords_current_shape[2].Y-=2;

		}
		else if(turn_shape == 1)
		{
			coords_current_shape[0].X--;
			coords_current_shape[0].Y++;
			coords_current_shape[3].X++;
			coords_current_shape[3].Y--;
			coords_current_shape[2].X+=2;

		}
		else if(turn_shape == 2)
		{
			coords_current_shape[0].X--;
			coords_current_shape[0].Y--;
			coords_current_shape[3].X++;
			coords_current_shape[3].Y++;
			coords_current_shape[2].Y+=2;

		}
		else if(turn_shape == 3)
		{
			coords_current_shape[0].X++;
			coords_current_shape[0].Y--;
			coords_current_shape[3].X--;
			coords_current_shape[3].Y++;
			coords_current_shape[2].X-=2;

		}
		if(turn_shape < 3) turn_shape++;
		else turn_shape = 0;

		restore_fig();
		break;
	case z_shape:

		if(turn_shape == 0)
		{
			if(data_mas_fig[coords_current_shape[0].Y+2][coords_current_shape[0].X] == PASSIVE_FIGURE ||
				data_mas_fig[coords_current_shape[1].Y][coords_current_shape[1].X+2] == PASSIVE_FIGURE||
				coords_current_shape[1].X+2 > 9){broke = true; break;} 
		}
		else
		{
			if(data_mas_fig[coords_current_shape[0].Y-2][coords_current_shape[0].X] == PASSIVE_FIGURE ||
				data_mas_fig[coords_current_shape[1].Y][coords_current_shape[1].X-2] == PASSIVE_FIGURE||
				coords_current_shape[1].X-2 < 0){broke = true; break;}
		}
		if(broke) break;
		del_fig();

		if(turn_shape == 0)
		{
			coords_current_shape[0].Y+=2;
			coords_current_shape[1].X+=2;
		}
		else
		{
			coords_current_shape[0].Y-=2;
			coords_current_shape[1].X-=2;
		}


		restore_fig();

		turn_shape = turn_shape ==1?0:1;
	}

}

void Figures::del_fig()
{
	for(int i = 0; i < 4;i++)
		data_mas_fig[coords_current_shape[i].Y][coords_current_shape[i].X] = SPACE;
}

void Figures::restore_fig()
{
	for(int i = 0; i < 4;i++)
		data_mas_fig[coords_current_shape[i].Y][coords_current_shape[i].X] = ACTIVE_FIGURE;
}
int Figures::get_FPS()const 
{
	return FPS;
}

int Figures::get_time_step()const
{
	return time_per_step;
}
void Figures::set_time_step(int step)
{
	time_per_step = step;
}
