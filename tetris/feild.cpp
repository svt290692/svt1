#include <glut.h>
#include "field.h"
#include <Windows.h>

Field::Field(GLint size_width,GLint size_height)
	  {
		  full_size_height = size_height; // ������ ������ ���� ��� �������� ��������������� � ������
		  full_size_width = size_width;
		  size_qube = (((full_size_height)/100) * 4 ); // ������ ������ ������
		  game_field_width = size_qube*10;// ������� �������� ���� ����������� �� ������� ������� ������ 10 ����� ������� 20 �����
		  game_field_height = size_qube*20 ;
		  fill_mass_coords(mass_coord_qubes);
	  }

void Field::draw()
{
	const COORD left_bottom_bckround = {full_size_width/2 - game_field_width/2 - ((game_field_width/100) * 30),(full_size_height/2 + game_field_height/2 + ((game_field_height/100) * 5))};
	const COORD left_top_bckround ={full_size_width/2 - game_field_width/2 - ((game_field_width/100) * 30),(full_size_height/2 - game_field_height/2 - ((game_field_height/100) * 5))};
	const COORD right_top_bckround = {full_size_width/2 + game_field_width/2 + ((game_field_width/100) * 30),(full_size_height/2 - game_field_height/2 - ((game_field_height/100) * 5))};
	const COORD right_bottom_bckround = {full_size_width/2 + game_field_width/2 + ((game_field_width/100) * 30),(full_size_height/2 + game_field_height/2 + ((game_field_height/100) * 5))};
	const COORD left_top_game_field = {full_size_width/2 - game_field_width/2,full_size_height/2 - game_field_height/2};
	const COORD left_bottom_game_field ={full_size_width/2 - game_field_width/2,full_size_height/2 + game_field_height/2};
	const COORD right_bottom_game_field = {full_size_width/2 + game_field_width/2,full_size_height/2 + game_field_height/2};
	const COORD right_top_game_field = {full_size_width/2 + game_field_width/2,full_size_height/2 - game_field_height/2};
	// ���� ������ ���������� � ������������ �� �������� ����
	glColor3f(0,0.8,0.1);
	// ���� ��������� ������� ����� �������� ���� � ������ ����� ��� ���������
	glBegin(GL_QUADS);
	glVertex2i(left_top_bckround.X,left_top_bckround.Y);
	glVertex2i(left_bottom_bckround.X,left_bottom_bckround.Y);
	glVertex2i(right_bottom_bckround.X,right_bottom_bckround.Y);
	glVertex2i(right_top_bckround.X,right_top_bckround.Y);
	glEnd();
	glLoadIdentity();

	glColor3f(0,1,1);
	glBegin(GL_LINE_LOOP);
	glVertex2i(left_top_bckround.X,left_top_bckround.Y);
	glVertex2i(left_bottom_bckround.X,left_bottom_bckround.Y);
	glVertex2i(right_bottom_bckround.X,right_bottom_bckround.Y);
	glVertex2i(right_top_bckround.X,right_top_bckround.Y);
	glEnd();
	glLoadIdentity();

	glColor3f(1,1,1);
	glBegin(GL_QUADS);
	glVertex2i(left_top_game_field.X,left_top_game_field.Y);
	glVertex2i(left_bottom_game_field.X,left_bottom_game_field.Y);
	glVertex2i(right_bottom_game_field.X,right_bottom_game_field.Y);
	glVertex2i(right_top_game_field.X,right_top_game_field.Y);
	glEnd();
	glLoadIdentity();

	glColor3f(0,0,0);
	glBegin(GL_LINE_LOOP);
	glVertex2i(left_top_game_field.X,left_top_game_field.Y);
	glVertex2i(left_bottom_game_field.X,left_bottom_game_field.Y);
	glVertex2i(right_bottom_game_field.X,right_bottom_game_field.Y);
	glVertex2i(right_top_game_field.X,right_top_game_field.Y);
	glEnd();
	glLoadIdentity();
	
}

void Field::draw_test_field()
{
	glColor3f(1,0,0);
	glBegin(GL_LINES);
	// �����..    ������ ������ ���� �������� ������� 
	for(GLint iter = (full_size_width/2 - game_field_width/2)+size_qube;iter <= full_size_width/2+(size_qube*4);iter += size_qube)
	{
		glVertex2i(iter,full_size_height/2 + game_field_height/2);
		glVertex2i(iter,full_size_height/2 - game_field_height/2);
	}
	glEnd();

	glLoadIdentity();

	glBegin(GL_LINES);
	// �����..    ������ ������ ���� �������� ������� 
	for(GLint iter = (full_size_height/2 - game_field_height/2)+size_qube;iter <= full_size_height/2+(size_qube*9);iter += size_qube)
	{
		glVertex2i(full_size_width/2 + game_field_width/2,iter);
		glVertex2i(full_size_width/2 - game_field_width/2,iter);
	}
	glEnd();

	glLoadIdentity();
}

void Field::fill_mass_coords(COORD (*mass)[10]) const 
{
	const GLint step = size_qube;
	for(GLint iter_y = (full_size_height/2 - game_field_height/2),y = 0;iter_y <= (full_size_height/2 + game_field_height/2) && y < 20;iter_y += step,y++)
	{
		for(int iter_x = full_size_width/2 - game_field_width/2,x = 0;iter_x <= full_size_width/2 + game_field_width/2 && x < 10;iter_x +=step,x++)
		{
			mass[y][x].X = iter_x;
			mass[y][x].Y = iter_y;
		}

	}

}

void Field::set_size(GLint width, GLint height)
{
	full_size_height = height;
	full_size_width = width;
}

