#include <stdio.h>
#include <stdlib.h>
#include <string>

typedef struct sign_node
{
	char sign;
	int count;
	char ciper[64];
	struct sign_node * left_node;
	struct sign_node * right_node;

} SYM,*NODE_POINT;

FILE *file_write = fopen("list_sym.txt","w+");
FILE *file_exit = fopen("exit_haff.txt","w+");
int count_regulator = 0;

void create_node_tree(NODE_POINT *root,char sign); // ������� ���������� �� �������
void create_signs_tree(NODE_POINT *root,FILE *file);// ������� �������� ������ �������� ��������� �� ����� � �� ��������
void print_tree(NODE_POINT root);// ������ ������
void create_counts_tree(NODE_POINT *root,char sign,int count,NODE_POINT left,NODE_POINT right);//������ ���������� ��� �������� �������� �� ��������� � �� �� ��������
void nulling_tree(NODE_POINT *root);// ������� ��������� ������ ������
void rebild_tree(NODE_POINT *root,NODE_POINT *new_root);// ������� ������������ ������
NODE_POINT destribution(NODE_POINT *root); // ������� ������������� ���������� �� ����� � ������ ������
void filling_massive(NODE_POINT *massive_to_filling,NODE_POINT root_of_tree);
void print_massive_node(NODE_POINT *massive); // ������ ������������ �������
int strlen_SYM(NODE_POINT *massive);
void sort_massive(NODE_POINT *massive,int size_mas);
void del_elm_mas(NODE_POINT *massive,int elm);
NODE_POINT create_haffman_node(NODE_POINT left,NODE_POINT right);
NODE_POINT building_haffman(NODE_POINT *massive);
void create_ciper(NODE_POINT root,char *sign);
void print_haf(NODE_POINT root);
NODE_POINT seek_sign(NODE_POINT root,char sign);

int main(int argc,char*argv[])
{
	NODE_POINT root = NULL;
	FILE *file_read = fopen("to.txt","rt");
	NODE_POINT sort_tree[256] = {NULL};

	create_signs_tree(&root,file_read); // �������� ������ �������� �� �����

	root = destribution(&root); // �������� ���������������� ������ � ���������� ����������� �� �����������

	filling_massive(sort_tree,root); // ���������� ������� �� ����������� �� ������

	nulling_tree(&root); // ��������� ������ �� �������������
	root = NULL;
	print_massive_node(sort_tree);// ������ �������

	root = building_haffman(sort_tree);

	print_haf(root);

	create_ciper(root,"");


	fclose(file_read);
	fclose(file_write);
}

NODE_POINT seek_sign(NODE_POINT root,char sign)
{
	if(root)
	{
		seek_sign(root->left_node,sign);
		seek_sign(root->right_node,sign);
		if(root->sign == sign) return root;
	}

}


void create_haffman_output(NODE_POINT root, FILE *file)
{
	//char buf_string[1000] = {'\0'};
	char bit_mask = 0;
	char sign;
	int offset = 0;
	int i;
//	char buf_haffman[8000] = {'\0'};
	NODE_POINT point_sign = NULL;

	while(!feof(file))
	{
		sign = fgetc(file);
		point_sign = seek_sign(root,sign);

		for(i = 0;i < strlen(point_sign->ciper);i++)
		{
			if(offset > 7)
			{
				fputc(bit_mask,file_exit);
				offset = 0;
				bit_mask = 0;
			}

			if(point_sign->ciper[i] == '1')


		}


	}


}


void print_haf(NODE_POINT root)
{
	if(root)
	{
		print_haf(root->left_node);
		printf("%d",root->count);
		print_haf(root->right_node);

	}

}

void create_ciper(NODE_POINT root,char *sign)
{
	
	if(root)
	{
		strcat(root->ciper,sign);
		if(root->left_node)strcpy(root->left_node->ciper,root->ciper);
		if(root->right_node)strcpy(root->right_node->ciper,root->ciper);
		create_ciper(root->left_node,"0");
		create_ciper(root->right_node,"1");

	}

}



NODE_POINT building_haffman(NODE_POINT *massive)
{
	int size_elm = strlen_SYM(massive);

	while(size_elm >= 1)
	{
		massive[0] = create_haffman_node(massive[0],massive[1]);
		del_elm_mas(massive,1);
		sort_massive(massive,size_elm--);
	}
	return massive[0];
}

NODE_POINT create_haffman_node(NODE_POINT left,NODE_POINT right)
{
	if(left && right)
	{
	NODE_POINT new_node = (NODE_POINT)malloc(sizeof(sign_node));
	new_node->count = (left->count + right->count);
	new_node->sign = NULL;
	memset(new_node->ciper,'\0',63);
	new_node->left_node = left;
	new_node->right_node = right;
	return new_node;
	}
	else {printf("error haf"); exit(2);}
}


int strlen_SYM(NODE_POINT *massive)
{
	int count = 0;
	while(massive[count])
		count++;
	return count-1;
}

void del_elm_mas(NODE_POINT *massive,int elm)
{
	int i;

	//free(massive[elm]);
	for( i = elm ; i < 256;i++)
	{
		massive[i] = massive[i+1];
	}

}


void sort_massive(NODE_POINT *massive,int size_mas)
{
	int i,i1,flag = 1;
	NODE_POINT temp;

	for(i = 0,i1 = 0,flag = 0;i < size_mas-1;i++)
	{
		if(massive[i]->count > massive[i+1]->count)
		{
			temp = massive[i];
			massive[i] = massive[i+1];
			massive[i+1] = temp;
		}


	}

}


void print_massive_node(NODE_POINT *massive)
{
	int i;
	for(i = 0;massive[i];i++)
	{
		fprintf(file_write,"%c-%d\n",massive[i]->sign,massive[i]->count);
	}
}


void filling_massive(NODE_POINT *massive_to_filling,NODE_POINT root_of_tree)
{
	if(root_of_tree)
	{
		filling_massive(massive_to_filling,root_of_tree->left_node);
		massive_to_filling[count_regulator] = (NODE_POINT)malloc(sizeof(sign_node));
		//massive_to_filling[count_regulator] = *root_of_tree;
		massive_to_filling[count_regulator]->count = root_of_tree->count;
		massive_to_filling[count_regulator]->sign = root_of_tree->sign;
		massive_to_filling[count_regulator]->left_node = NULL;
		massive_to_filling[count_regulator++]->right_node = NULL;
		//massive_to_filling[count_regulator]->count = 0;
		filling_massive(massive_to_filling,root_of_tree->right_node);
	}
}


void print_tree(NODE_POINT root)
{
	if(root)
	{
		print_tree(root->left_node);
		fprintf(file_write,"%c--->%d\n",root->sign,root->count);
		print_tree(root->right_node);
	}
}


void create_signs_tree(NODE_POINT *root,FILE *file)
{
	char sign;
	if(file)
	{
		while(!feof(file))
		{
			sign = fgetc(file);
			if(sign != -1)
			create_node_tree(root,sign);
		}
	}
	else
	{
		printf("error riding file");
		exit(1);
	}
}


void create_node_tree(NODE_POINT *root,char sign)
{
	if(*root == NULL)
	{
		*root = (NODE_POINT)malloc(sizeof(sign_node));
		(*root)->left_node = NULL;
		(*root)->right_node = NULL;
		(*root)->sign = sign;
		(*root)->count = 1;
	}
	else if((*root)->sign > sign)
		create_node_tree(&(*root)->left_node,sign);
	else if((*root)->sign < sign)
		create_node_tree(&(*root)->right_node,sign);
	else (*root)->count++;
}


void create_counts_tree(NODE_POINT *root,char sign,int count,NODE_POINT left,NODE_POINT right)
{
		if(*root == NULL)
	{
		*root = (NODE_POINT)malloc(sizeof(sign_node));
		(*root)->left_node = left;
		(*root)->right_node = right;
		(*root)->sign = sign;
		(*root)->count = count;
		memset((*root)->ciper,'\0',63);
	}
	else if((*root)->count >= count)
		create_counts_tree(&(*root)->left_node,sign,count,left,right);
	else if((*root)->count < count)
		create_counts_tree(&(*root)->right_node,sign,count,left,right);
	
}


void rebild_tree(NODE_POINT *root,NODE_POINT *new_root)
{
	if(*root)
	{
		rebild_tree(&(*root)->left_node,new_root);
		rebild_tree(&(*root)->right_node,new_root);
		create_counts_tree(new_root,(*root)->sign,(*root)->count,NULL,NULL);
		free(*root);
	}
}


NODE_POINT destribution(NODE_POINT *root)
{
	NODE_POINT new_root = NULL;

	rebild_tree(root,&new_root);

	return new_root;
}


void nulling_tree(NODE_POINT *root)
{
	if(*root)
	{
		nulling_tree(&(*root)->left_node);
		nulling_tree(&(*root)->right_node);
		free(*root);
	}
}
