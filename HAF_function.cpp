#include "HAF_header.h"

void file_decomp_ini(FILE *file_in,FILE *file_out)
{
	char sign;
	char buffer_ini[70000] = {'\0'};
	char temp_bufer[10000] = {'\0'};
	int i,count_mas = 0;
	unsigned long long last_sym;
	int tail;
	NODE_POINT root = NULL;
	NODE_POINT massive_sign[256] = {NULL};


	//sign = fgetc(file_in);
	//if(feof(file_in)) {printf("error file not correct"); exit(2);}

	fgets(buffer_ini,70000,file_in);
	if(buffer_ini[strlen(buffer_ini)-1] == '\n') fgets(temp_bufer,10000,file_in);
	strcat(buffer_ini,temp_bufer);

	if(buffer_ini[strlen(buffer_ini)-1] == '\n') fgets(temp_bufer,10000,file_in);
	strcat(buffer_ini,temp_bufer);
		
	for(i = 0;i < strlen(buffer_ini);i++)
	{
		
		if(buffer_ini[i] == '/' && buffer_ini[i+1] == '/' && buffer_ini[i+2] == '/')
		{
			i+=3;
			//last_sign = atoi(&buffer_ini[i]);
			//while(buffer_ini[i] != ',') i++;
			tail = buffer_ini[i] -'0';
			break;
		}
		else
		{
			massive_sign[count_mas] = (NODE_POINT)malloc(sizeof(SYM));
			*massive_sign[count_mas]->ciper = '\0';
			massive_sign[count_mas]->left_node = NULL;
			massive_sign[count_mas]->right_node = NULL;
			massive_sign[count_mas]->sign = buffer_ini[i++];
			massive_sign[count_mas]->count = atoi(&buffer_ini[i]);
			count_mas++;
			while(buffer_ini[i] != '*') i++;
		}
	}

	root = building_haffman(massive_sign);
	create_ciper(root,"");
	decoder(root,tail,file_in);
	
	
}


NODE_POINT seek_sign(NODE_POINT root,char sign)
{
	NODE_POINT temp;
	if(!root)
		return NULL;

		if(root->sign == sign) 
			return root;
		if(root->left_node) 
		{
			temp = seek_sign(root->left_node,sign);
			if(temp==NULL)
			   return seek_sign(root->right_node,sign);
		}
		else
			return seek_sign(root->right_node,sign);

}

void decoder(NODE_POINT root,int teil,FILE *file)
{

	int i;
//	int count_sign = 0;
	char bit_mask = 0;
	char control = 0;
	int offset = 7;
	NODE_POINT point_seek = root;
	//fclose(file);
	//file = fopen("exit_haff.txt","rb");
		rewind(file);
		bit_mask = fgetc(file);
		control = fgetc(file);
		//count_sign++;
		
	while(1)
	{
		bit_mask = fgetc(file);
		if(bit_mask == '/')
		{
			bit_mask = fgetc(file);
			if(bit_mask == '/')
			{
				bit_mask = fgetc(file);
				if(bit_mask == '/')
				{
					bit_mask = fgetc(file);
					bit_mask = fgetc(file);
					control = fgetc(file);break;}}}}

	while(1)
	{
		if(offset < 0)
		{
			offset = 7;
			bit_mask = control;
			control = fgetc(file);
			//count_sign++;

		}


	if(feof(file))
	{
			for(i = 0;i < (teil == 0? 8 : teil);i++)
			{
				if(bit_mask & (1<<offset))
				{
				fputc('1',file_temp1);
				point_seek = point_seek->right_node;
				offset--;
				}
				else {point_seek = point_seek->left_node; offset--;fputc('0',file_temp1);}

				if(point_seek->sign != 0)
				{
				fputc(point_seek->sign,file_decoder);
				if(point_seek->sign == '\n')
					fprintf(file_decoder,"\n");
				point_seek = root;
				}

			}
			break;
	}
	else
	{
		if(bit_mask & (1<<offset))
		{
			fputc('1',file_temp1);
			point_seek = point_seek->right_node;
			offset--;
		}
		else {point_seek = point_seek->left_node; offset--;fputc('0',file_temp1);}

		if(point_seek->sign != 0)
		{
			fputc(point_seek->sign,file_decoder);
			point_seek = root;
		}



	}
	}


}



int create_haffman_output(NODE_POINT root, FILE *file)
{
	char bit_mask = 0;
	char buf[100] = {'\0'};
	int sign;
	unsigned long long count = 0;
	int i;
	short tail = 0;
	short offset = 7;
	

	NODE_POINT point_sign = NULL;
	rewind(file);

	//sign = fgetc(file);
	

	while(!feof(file) )
	{
		sign = fgetc(file);
		if(!feof(file)){

			
		point_sign = seek_sign(root,(char)sign);

		for(i = 0;i < strlen(point_sign->ciper);i++)
		{
			if(point_sign->ciper[i] == '1'){ fputc('1',file_temp); count++; }
			else {fputc('0',file_temp);count++;}
		}


		}
	

	}
	tail = (count % 8);

	fprintf(file_exit,"%s",list_numbers);
	fprintf(file_exit,"\/\/\/%d",tail == 0? 0 :tail);

	sign = 0;
	rewind(file_temp);

	while(!feof(file_temp))
	{
		sign = fgetc(file_temp);

		if(!feof(file_temp))
		{
			if(sign  == '1'){ bit_mask |= 1<<offset;offset--;}
			else {offset--;}
		}
		if(offset < 0){ fputc(bit_mask,file_exit);offset = 7; bit_mask = 0;}
		//count--;
	}
	if(offset != 7) fputc(bit_mask,file_exit);

	return tail;
}


void print_haf(NODE_POINT root)
{
	if(root)
	{
		print_haf(root->left_node);
		printf("%d",root->count);
		print_haf(root->right_node);

	}

}

void create_ciper(NODE_POINT root,char *sign)
{
	
	if(root)
	{
		strcat(root->ciper,sign);
		if(root->left_node)strcpy(root->left_node->ciper,root->ciper);
		if(root->right_node)strcpy(root->right_node->ciper,root->ciper);
		create_ciper(root->left_node,"0");
		create_ciper(root->right_node,"1");

	}

}



NODE_POINT building_haffman(NODE_POINT *massive)
{
	int size_elm = strlen_SYM(massive);

	while(size_elm >= 1)
	{
		massive[0] = create_haffman_node(massive[0],massive[1]);
		del_elm_mas(massive,1);
		sort_massive(massive,size_elm--);
	}
	return massive[0];
}

NODE_POINT create_haffman_node(NODE_POINT left,NODE_POINT right)
{
	if(left && right)
	{
	NODE_POINT new_node = (NODE_POINT)malloc(sizeof(sign_node));
	new_node->count = (left->count + right->count);
	new_node->sign = NULL;
	memset(new_node->ciper,'\0',63);
	new_node->left_node = left;
	new_node->right_node = right;
	return new_node;
	}
	else {printf("error haf"); exit(2);}
}


int strlen_SYM(NODE_POINT *massive)
{
	int count = 0;
	while(massive[count])
		count++;
	return count-1;
}

void del_elm_mas(NODE_POINT *massive,int elm)
{
	int i;

	//free(massive[elm]);
	for( i = elm ; i < 256;i++)
	{
		massive[i] = massive[i+1];
	}

}


void sort_massive(NODE_POINT *massive,int size_mas)
{
	int i,i1,flag = 1;
	NODE_POINT temp;

	for(i = 0,i1 = 0,flag = 0;i < size_mas-1;i++)
	{
		if(massive[i]->count > massive[i+1]->count)
		{
			temp = massive[i];
			massive[i] = massive[i+1];
			massive[i+1] = temp;
		}


	}

}


void print_massive_node(NODE_POINT *massive)
{
	int i,j = 0;
	char buf[10000] = {'\0'};
	for(i = 0;massive[i];i++)
	{
		//fprintf(file_write,"%c%d*",massive[i]->sign,massive[i]->count);
		sprintf(buf,"%c%d*",massive[i]->sign,massive[i]->count);
		strcat(list_numbers,buf);
	}
//	printf("%s",list_numbers);	

//	strcat(list_numbers,buf);
	//fprintf(file_exit,"///");
	//for(i = 0;i < 300;i++)
	//fprintf(file_exit," ");
}


void filling_massive(NODE_POINT *massive_to_filling,NODE_POINT root_of_tree)
{
	if(root_of_tree)
	{
		filling_massive(massive_to_filling,root_of_tree->left_node);
		massive_to_filling[count_regulator] = (NODE_POINT)malloc(sizeof(sign_node));
		//massive_to_filling[count_regulator] = *root_of_tree;
		massive_to_filling[count_regulator]->count = root_of_tree->count;
		massive_to_filling[count_regulator]->sign = root_of_tree->sign;
		massive_to_filling[count_regulator]->left_node = NULL;
		massive_to_filling[count_regulator++]->right_node = NULL;
		//massive_to_filling[count_regulator]->count = 0;
		filling_massive(massive_to_filling,root_of_tree->right_node);
	}
}


void create_signs_tree(NODE_POINT *root,FILE *file)
{
	char sign;
	if(file)
	{
		while(!feof(file))
		{
			sign = fgetc(file);
			if(!feof(file))
		//	if(sign == -1)
				//sign = 255;
			create_node_tree(root,sign);
		}
	}
	else
	{
		printf("error riding file");
		exit(1);
	}
}


void create_node_tree(NODE_POINT *root,char sign)
{
	if(*root == NULL)
	{
		*root = (NODE_POINT)malloc(sizeof(sign_node));
		(*root)->left_node = NULL;
		(*root)->right_node = NULL;
		(*root)->sign = sign;
		(*root)->count = 1;
	}
	else if((*root)->sign > sign)
		create_node_tree(&(*root)->left_node,sign);
	else if((*root)->sign < sign)
		create_node_tree(&(*root)->right_node,sign);
	else (*root)->count++;
}


void create_counts_tree(NODE_POINT *root,char sign,int count,NODE_POINT left,NODE_POINT right)
{
		if(*root == NULL)
	{
		*root = (NODE_POINT)malloc(sizeof(sign_node));
		(*root)->left_node = left;
		(*root)->right_node = right;
		(*root)->sign = sign;
		(*root)->count = count;
		memset((*root)->ciper,'\0',63);
	}
	else if((*root)->count >= count)
		create_counts_tree(&(*root)->left_node,sign,count,left,right);
	else if((*root)->count < count)
		create_counts_tree(&(*root)->right_node,sign,count,left,right);
	
}


void rebild_tree(NODE_POINT *root,NODE_POINT *new_root)
{
	if(*root)
	{
		rebild_tree(&(*root)->left_node,new_root);
		rebild_tree(&(*root)->right_node,new_root);
		create_counts_tree(new_root,(*root)->sign,(*root)->count,NULL,NULL);
		free(*root);
	}
}


NODE_POINT destribution(NODE_POINT *root)
{
	NODE_POINT new_root = NULL;

	rebild_tree(root,&new_root);

	return new_root;
}


void nulling_tree(NODE_POINT *root)
{
	if(*root)
	{
		nulling_tree(&(*root)->left_node);
		nulling_tree(&(*root)->right_node);
		free(*root);
	}
}
