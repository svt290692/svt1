#pragma once
#include <iostream>
#include <time.h>

class Matrix
{
public:
	Matrix(void);
	Matrix(int strings,int columns);
	Matrix(Matrix& other);
	Matrix(Matrix&& other);
	~Matrix(void);
	Matrix operator + (int val);
	Matrix&& operator - (int val);
	Matrix operator / (int val);
	Matrix operator * (int val);

	Matrix operator + (Matrix& other);
	Matrix operator - (Matrix& other);
	Matrix operator * (Matrix& other);
	Matrix& operator = (Matrix& other);
	bool operator != (Matrix& other);
	bool operator == (Matrix& other);
	int* operator [] (int val);

	int strings(){return size_strings;}
	int columns(){return size_columns;}

	void rand_gen(int max);
	void nulling();

private:
	int size_strings, size_columns;
	int **I_AM;
};

std::ostream& operator << (std::ostream& out,Matrix &matr);