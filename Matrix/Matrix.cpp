#include "Matrix.h"

	std::ostream& operator << (std::ostream& out,Matrix &matr)
	{
		for(int i = 0 ; i < matr.strings() ; i ++ )
		{
			for(int j = 0 ; j < matr.columns() ; j ++ )
			{
				out<<matr[i][j]<<' ';
			}
			out<<std::endl;
		}
		return out;
	}

	Matrix::Matrix(void)
	{
		size_columns = 0;
		size_strings = 0;
		I_AM = nullptr;
		nulling();
	}

	Matrix::Matrix(int strings,int columns)
	{
		if(strings < 1 || columns < 1) throw std::exception();

		I_AM = new int*[strings];

		for(int i = 0 ; i < strings ; i ++ )
		{
			I_AM[i] = new int[columns];
		}
		size_columns = columns;
		size_strings = strings;
		nulling();
	}

	Matrix::Matrix(Matrix& other)
	{
		size_columns = other.size_columns;
		size_strings = other.size_strings;

		I_AM = new int*[size_strings];

		for(int i = 0 ; i < size_strings ; i ++ )
		{
			I_AM[i] = new int[size_columns];
		}

		for(int i = 0 ; i < strings() ; i ++ )
		{
			for(int j = 0 ; j < columns() ; j ++ )
			{
				I_AM[i][j] = other[i][j];
			}
		}
	}

	Matrix::Matrix(Matrix&& other)
	{
		I_AM = std::move(other.I_AM);

		for(int i = 0 ; i < other.size_strings ; i ++ )
		{
			I_AM[i] = std::move(other.I_AM[i]);
		}
		size_strings = other.size_strings;
		size_columns = other.size_columns;

	}

	Matrix::~Matrix(void)
	{
		for(int i = size_strings-1; i >= 0 ; i -- )
		{
			delete[] I_AM[i];
		}
		delete[] I_AM;
	}

	Matrix Matrix::operator + (int val)
	{
		Matrix temp(strings(),columns());

		for(int i = 0 ; i < strings() ; i ++ )
		{
			for(int j = 0 ; j < columns() ; j ++ )
			{
				temp[i][j] = I_AM[i][j] + val;
			}
		}
		return temp;
	}

	Matrix&& Matrix::operator - (int val)
	{
		Matrix temp(strings(),columns());

		return temp + (-val);
	}

	Matrix Matrix::operator / (int val)
	{
		if(val == 0) throw std::exception();

		Matrix temp(strings(),columns());

		for(int i = 0 ; i < strings() ; i ++ )
		{
			for(int j = 0 ; j < columns() ; j ++ )
			{
				temp[i][j] = I_AM[i][j] / val;
			}
		}
		return temp;
	}

	Matrix Matrix::operator * (int val)
	{
		Matrix temp(strings(),columns());

		for(int i = 0 ; i < strings() ; i ++ )
		{
			for(int j = 0 ; j < columns() ; j ++ )
			{
				temp[i][j] = I_AM[i][j] * val;
			}
		}
		return temp;
	}


	Matrix Matrix::operator + (Matrix& other)
	{
		int str = other.strings() > strings()?other.strings():strings();
		int col = other.columns() > columns()?other.columns():columns();

		Matrix temp(str,col);

		for(int i = 0 ; i < str ; i ++ )
		{
			for(int j = 0 ; j < col ; j ++ )
			{
				if(i <= other.strings() && j <= other.columns())temp[i][j] += I_AM[i][j];
				if(i <= other.strings() && j <= other.columns())temp[i][j] += other[i][j];
			}
		}
		return temp;
	}

	Matrix Matrix::operator - (Matrix& other)
	{
		return (*this + ( other * (-1)));
	}

	Matrix Matrix::operator * (Matrix& other)
	{
		if(other.strings() != columns()) throw std::exception("the matrix is not correct to multiplication");

		Matrix temp(strings(),columns());

		for(int i = 0 ; i < temp.strings() ; i ++ )// ���� ��������� ���������
		{
			for(int j = 0 ; j < temp.columns() ; j ++ ) // ����������� �� �������� 
			{
				for(int k = 0 ; k < temp.columns() ; k++)
					temp[i][j] += (I_AM[i][k] * other[k][j]);
			}
		}
		return temp;
	}

	//�������� ������������ ������ ������ �������� �������� ��������� ������� ����� ������������ �� ����� �������
	Matrix& Matrix::operator = (Matrix& other)
	{
		for(int i = size_strings-1; i >= 0 ; i -- )
		{
			delete[] I_AM[i];
		}
		delete[] I_AM;

		size_columns = other.size_columns;
		size_strings = other.size_strings;

		I_AM = new int*[size_strings];

		for(int i = 0 ; i < size_strings ; i ++ )
		{
			I_AM[i] = new int[size_columns];
		}
		return *this;
	}

	bool Matrix::operator != (Matrix& other)
	{
		if(!(*this == other)) return true;
		else return false;
	}

	bool Matrix::operator == (Matrix& other)
	{
		for(int i = 0 ; i < strings() ; i ++ )
		{
			for(int j = 0 ; j < columns() ; j ++ )
			{
				if(I_AM[i][j] != other[i][j]) return false;
			}
		}
		return true;
	}

	int* Matrix::operator [] (int val)
	{
		if(val < size_strings)
		return I_AM[val];
		else throw std::exception("index out of range in Matrix");
	}

	void Matrix::rand_gen(int max)
	{

		for(int i = 0 ; i < size_strings ; i ++ )
		{
			for(int j = 0 ; j < size_columns ; j ++ )
			{
				I_AM[i][j] = (std::rand() % max);
			}
		}
	}
	void Matrix::nulling()
	{
		for(int i = 0 ; i < size_strings ; i ++ )
		{
			for(int j = 0 ; j < size_columns ; j ++ )
			{
				I_AM[i][j] = 0;
			}
		}
	}
