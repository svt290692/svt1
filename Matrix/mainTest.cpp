#include <iostream>
#include "Matrix.h"

using std::cout;
using std::endl;
int main(int argc,char** argv)
{
	std::srand(unsigned(time(NULL)));

	Matrix A(5,5);
	Matrix B(5,5);
	Matrix C(5,5);
	Matrix D(5,5);
	A.rand_gen(10);
	B.rand_gen(10);
	C.rand_gen(10);
	D.rand_gen(10);

	std::cout<<"5 to 5 A"<<std::endl<<A<<std::endl;
	std::cout<<"5 to 5 B"<<std::endl<<B<<std::endl;

	cout<<"A + B = "<<endl<< (A+B) <<endl;
	cout<<"A - B = "<<endl<<(A-B)<<endl;
	cout<<"A * B = "<<endl<<(A*B)<<endl;

	cout<<"A * 5 = "<<endl<< (A * 5 )<<endl;
	cout<<"B * 5 = "<<endl<< (B * 5 )<<endl;
	cout<<"(A + B) * 5 = "<<endl<< ( A + B ) * 5 <<endl;

	return 0;
}
