#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <Windows.h>
#include <conio.h>
#define ARROWS 224
#define DOWN_ARROW 80
#define UP_ARROW 72
#define LEFT_ARROW 75
#define RIGTHT_ARROW 77
#define SPACE 32
#define ZERO 2
#define PLAYER_VS_X 4
#define PLAYER_VS_O 3
#define PLAYER_VS_PLAYER 1

HANDLE desc_out = GetStdHandle(STD_OUTPUT_HANDLE); // ���������� ������
const COORD coord_game_for_robot[3][3] = {{{36,8},{38,8},{40,8}},{{36,10},{38,10},{40,10}},{{36,12},{38,12},{40,12}}}; // ���������� ����� ��� ������

enum // ����� ��� ���� � ������
{
	black = 0,
	dark_blue,
	dark_green,
	dark_sky_blue,
	dark_red,
	dark_purple,
	dark_yellow,
	dark_white,
	gray,
	blue,
	green,
	sky_blue,
	red,
	purple,
	yellow,
	white
};


void go_to_coord(size_t x,size_t y); // ������� �������� �� ����������� � �������
void set_color_txt_back(size_t text,size_t back); // ������� ��������� ����� ���� � ������ � �������
size_t UTIL_randomNum(size_t _iMin, size_t _iMax); // ������������ �����
size_t menu(); // ��������� ����
void print_game(); // ������ ���� ��� ����
size_t get_game(size_t virt_space[3][3],size_t choise); // ���� ����
size_t robot_stroke(size_t who_robot,size_t *player, size_t (*virt_space)[3]); // ������� ���� ������
size_t check(size_t (*field)[3]); // �������� ��������
size_t ask_keep_game(); // ������ � ����������� ����
void set_color_ground_console(HANDLE descript_out,size_t text,size_t background); //������� ������� ����� � ����������� ���������� ����������� 
void rus_text_printf_string(char *from); // ������� ������ ������ �������� ������ � �������

size_t main()
{
	size_t decision = 0,choise;
	size_t virtul_space[3][3] = {0};
	memset(virtul_space,0,sizeof(size_t)*9);
	do
	{
		if(decision)
		{
		memset(virtul_space,0,sizeof(size_t)*9);
		set_color_ground_console(desc_out,white,black);
		system("CLS");
		fflush(stdin);
		fflush(stdout);
		}
	choise = menu();

	set_color_ground_console(desc_out,white,black);
	system("CLS");
	fflush(stdin);
	fflush(stdout);

	print_game();
	decision = get_game(virtul_space,choise);


	}
	while(decision == 2);
	return 0;

}

void go_to_coord(size_t x,size_t y)
{
	COORD coord = {x,y};
	SetConsoleCursorPosition(desc_out,coord);
}

void set_color_txt_back(size_t text,size_t back)
{
	SetConsoleTextAttribute(desc_out,((WORD)text | (back << 4)));
}

void print_game()
{
	go_to_coord(35,7);
	set_color_txt_back(dark_green,white);
	printf("%c%c%c%c%c%c%c",201,205,203,205,203,205,187);
	go_to_coord(35,8);
	printf("%c%c%c%c%c%c%c",186,' ',186,' ',186,' ',186);
	go_to_coord(35,9);
	printf("%c%c%c%c%c%c%c",204,205,206,205,206,205,185);
	go_to_coord(35,10);
	printf("%c%c%c%c%c%c%c",186,' ',186,' ',186,' ',186);
	go_to_coord(35,11);
	printf("%c%c%c%c%c%c%c",204,205,206,205,206,205,185);
	go_to_coord(35,12);
	printf("%c%c%c%c%c%c%c",186,' ',186,' ',186,' ',186);
	go_to_coord(35,13);
	printf("%c%c%c%c%c%c%c",200,205,202,205,202,205,188);
	
	go_to_coord(30,14);
	printf("\xc9\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xBB");
	go_to_coord(30,15);
	printf("\xba");rus_text_printf_string("��� ���������.");printf("\xba");
	go_to_coord(30,16);
	printf("\xc8\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xBC");
}

size_t get_game(size_t (*virt_space)[3],size_t choise)
{
	size_t x_coord = 36,y_coord = 8,key = 0,x_axis = 0,y_axis = 0;
	size_t player = 1;
	unsigned short cheking = 0;
	go_to_coord(x_coord,y_coord);
	while(1)
	{
		cheking = check(virt_space);
		if(choise == PLAYER_VS_X && player == 1 && !cheking)
		{
			robot_stroke(player,&player,virt_space);
			go_to_coord(x_coord,y_coord);
			
		}
		else if(choise == PLAYER_VS_O && player == 2 && !cheking)
		{
			robot_stroke(player,&player,virt_space);
			go_to_coord(x_coord,y_coord);
			
		}
		else if(!cheking)
		{	

			


	key = _getch();
	if(key == ARROWS)
	{
 	//key = _getch();
	switch(_getch())
	{
	case UP_ARROW: 
		if(y_coord-2 >= 8)
		{
			y_axis--;
			y_coord -= 2;
			go_to_coord(x_coord,y_coord);
		}
		break;
	case DOWN_ARROW:
		if(y_coord+2 <= 12)
		{
			y_axis++;
			y_coord += 2;
			go_to_coord(x_coord,y_coord);
		}
		break;
	case LEFT_ARROW:
		if(x_coord-2 >= 36)
		{
			x_axis--;
			x_coord -= 2;
			go_to_coord(x_coord,y_coord);
		}
		break;
	case RIGTHT_ARROW:
		if(x_coord+2 <= 40)
		{
			x_axis++;
			x_coord += 2;
			go_to_coord(x_coord,y_coord);
		}
		break;
		}
	}
	else if(key == SPACE)
	{
		if(!virt_space[y_axis][x_axis] && player == 1)
		{
		putchar('X');
		go_to_coord(x_coord,y_coord);
		virt_space[y_axis][x_axis] = 1;
		player = 2;
		go_to_coord(31,15);
		rus_text_printf_string("��� �������.  ");
		go_to_coord(x_coord,y_coord);
		}
		else if(!virt_space[y_axis][x_axis] && player == 2)
		{
			putchar('O');
		go_to_coord(x_coord,y_coord);
		virt_space[y_axis][x_axis] = ZERO;
		player = 1;
		go_to_coord(31,15);
		rus_text_printf_string("��� ���������.");
		go_to_coord(x_coord,y_coord);
		}
		}


		
			
	}
		if(cheking)
		{
		if(cheking == 1)
		{
			go_to_coord(30,6);
		set_color_ground_console(desc_out,green,dark_sky_blue);
		rus_text_printf_string("�������� ��������!");
		go_to_coord(0,22);
		return ask_keep_game();
		}
		else if(cheking == 2)
		{
			go_to_coord(30,6);
		set_color_ground_console(desc_out,green,dark_sky_blue);
		rus_text_printf_string("�������� ������!");
		go_to_coord(0,22);
		return ask_keep_game();
		}
		else if(cheking == 3)
		{
			go_to_coord(30,6);
		set_color_ground_console(desc_out,green,dark_sky_blue);
		rus_text_printf_string("�����...");
		go_to_coord(0,22);
		return ask_keep_game();
		}
		
		
	}
	}
	
	
	return 0;
}

size_t check(size_t (*field)[3])
{
	size_t i=0,i1=0,axis=0;
	short count = 0;
	for(i = 1;i <= 2;i++)
	{
		for(axis = 0;axis < 3;axis++)
		{
			if(field[axis][0] == i && field[axis][1] == i && field[axis][2] == i)
				return i;
			else if(field[0][axis] == i && field[1][axis] == i && field[2][axis] == i)
				return i;
			
		}
		if(field[0][0] == i && field[1][1] == i && field[2][2] == i)
			return i;
		else if(field[2][0] == i && field[1][1] == i && field[0][2] == i)
			return i;
		else {
			for(i1 = 0,count = 0;i1 < 9;i1++)
			{
				if(field[0][i1] > 0 && field[0][i1] < 3)
					count++;		
				else break;

			}
			if(count == 9)
				return 3;
		}
	}

	return 0;
}


size_t ask_keep_game()
{
	size_t key,flag = 2;

	Sleep(2000);
	go_to_coord(30,5);
	rus_text_printf_string("��� ������?"); putchar(2);
	go_to_coord(33,4);
	set_color_ground_console(desc_out,white,black);
	printf("[YES]");
	go_to_coord(39,4);
	printf("[NO]");

	go_to_coord(33,4);
	set_color_ground_console(desc_out,white,green);
	printf("[YES]");

	while(1)
	{
	key = _getch();
	if(key == ARROWS)
	{
		key = _getch();
		switch(key)
	{
	case RIGTHT_ARROW:
		go_to_coord(33,4);
	set_color_ground_console(desc_out,white,black);
	printf("[YES]");

	go_to_coord(39,4);
	set_color_ground_console(desc_out,white,green);
	printf("[NO]");
	flag = 1;
	break;
	case LEFT_ARROW:
		go_to_coord(39,4);
	set_color_ground_console(desc_out,white,black);
	printf("[NO]");

	go_to_coord(33,4);
	set_color_ground_console(desc_out,white,green);
	printf("[YES]");
	flag = 2;
	break;

	}
	
	}
	else if(key == 13)
		return flag;
	}

	
	return 0;
}

void set_color_ground_console(HANDLE descript_out,size_t text,size_t background)
{

	SetConsoleTextAttribute(descript_out,(text | (background << 4)));
}

void rus_text_printf_string(char *from)
{

	short string_size = strlen(from)+1;
	char *buffer = (char*)malloc(sizeof(char)*string_size);
	AnsiToOem(from,buffer);
	printf("%s",buffer);
	free(buffer);
}

size_t robot_stroke(size_t who_robot,size_t *player, size_t (*virt_space)[3])
{
	short opponent = (who_robot == 1 ? 2 : 1);
	short x_axis,y_axis,count = 0;
	int i,i0;

	for(y_axis = 0,count = 0; y_axis < 3;y_axis++) // �������� ���������
	{
		for(i = 0,count = 0; i < 3;i++)
		{
			if(virt_space[y_axis][i] == opponent) count++;

		}

		if(count == 2)
		{
			for(i0 = 0;i0 < 3;i0++)
			{
				if(virt_space[y_axis][i0] == 0) 
				{
					SetConsoleCursorPosition(desc_out,coord_game_for_robot[y_axis][i0]);
					virt_space[y_axis][i0] = who_robot;
					putchar(who_robot == 1? 'X' : 'O');
					go_to_coord(31,15);
					rus_text_printf_string(who_robot == 1?"��� �������.  ":"��� ���������");
					SetConsoleCursorPosition(desc_out,coord_game_for_robot[y_axis][i0]);
					*player = opponent;
					return opponent;				
				}

			}

		}


	}



	for(x_axis = 0,count = 0; x_axis < 3;x_axis++)// �������� �����������
	{
		for(i = 0,count = 0; i < 3;i++)
		{
			if(virt_space[i][x_axis] == opponent) count++;

		}

		if(count == 2)
		{
			for(i0 = 0;i0 < 3;i0++)
			{
				if(virt_space[i0][x_axis] == 0) 
				{
					SetConsoleCursorPosition(desc_out,coord_game_for_robot[i0][x_axis]);
					 virt_space[i0][x_axis] = who_robot;
					putchar(who_robot == 1? 'X' : 'O');
					go_to_coord(31,15);
					rus_text_printf_string(who_robot == 1?"��� �������.  ":"��� ���������");
					SetConsoleCursorPosition(desc_out,coord_game_for_robot[i0][x_axis]);
					*player = opponent;
					return opponent;				
				}

			}

		}
	}

	for(x_axis = 0,y_axis = 0,count = 0; x_axis < 3;x_axis++,y_axis++)
	{
		if(virt_space[y_axis][x_axis] == opponent) count++;

		if(count == 2)
		{
			for(x_axis = 0,y_axis = 0;x_axis < 3;x_axis++,y_axis++)
			{
				if(virt_space[y_axis][x_axis] == 0) 
				{
					SetConsoleCursorPosition(desc_out,coord_game_for_robot[y_axis][x_axis]);
					virt_space[y_axis][x_axis] = who_robot;
					putchar(who_robot == 1 ? 'X' : 'O');
					go_to_coord(31,15);
					rus_text_printf_string(who_robot == 1?"��� �������.  ":"��� ���������");
					SetConsoleCursorPosition(desc_out,coord_game_for_robot[y_axis][x_axis]);
					*player = opponent;
					return opponent;				
				}

			}

		}
	

	}


	for(x_axis = 0,y_axis = 2,count = 0; x_axis < 3;x_axis++,y_axis--)
	{
		if(virt_space[y_axis][x_axis] == opponent) count++;

		if(count == 2)
		{
			for(x_axis = 0,y_axis = 2;x_axis++ < 3;x_axis++,y_axis--)
			{
				if(virt_space[y_axis][x_axis] == 0) 
				{
					SetConsoleCursorPosition(desc_out,coord_game_for_robot[y_axis][x_axis]);
					virt_space[y_axis][x_axis] = who_robot;
					putchar(who_robot == 1 ? 'X' : 'O');
					go_to_coord(31,15);
					rus_text_printf_string(who_robot == 1?"��� �������.  ":"��� ���������");
					SetConsoleCursorPosition(desc_out,coord_game_for_robot[y_axis][x_axis]);
					*player = opponent;
					return opponent;				
				}

			}

		}
	

	}
	while(1)
	{
		y_axis = UTIL_randomNum(0,2);
		x_axis = UTIL_randomNum(0,2);
		if(virt_space[y_axis][x_axis] == 0)
		{
			SetConsoleCursorPosition(desc_out,coord_game_for_robot[y_axis][x_axis]);
			virt_space[y_axis][x_axis] = who_robot;
			putchar(who_robot == 1 ? 'X' : 'O');
			go_to_coord(31,15);
			rus_text_printf_string(who_robot == 1?"��� �������.  ":"��� ���������");
			SetConsoleCursorPosition(desc_out,coord_game_for_robot[y_axis][x_axis]);
			*player = opponent;
			return opponent;
		}

	}

					
}

size_t menu()
{
	size_t key,flag = 2;


	go_to_coord(30,5);
	set_color_ground_console(desc_out,white,black);
	rus_text_printf_string("�� ����� ������ � ����������� ��� � ������?");
	go_to_coord(33,4);
	set_color_ground_console(desc_out,white,black);
	rus_text_printf_string("[� ������]");
	go_to_coord(45,4);
	rus_text_printf_string("[����]");

	go_to_coord(33,4);
	set_color_ground_console(desc_out,white,green);
	rus_text_printf_string("[� ������]");


	while(1)
	{
	key = _getch();
	if(key == ARROWS)
	{
		key = _getch();
		switch(key)
	{
	case RIGTHT_ARROW:
		go_to_coord(33,4);
	set_color_ground_console(desc_out,white,black);
	rus_text_printf_string("[� ������]");

	go_to_coord(45,4);
	set_color_ground_console(desc_out,white,green);
	rus_text_printf_string("[����]");
	flag = 1;
	break;
	case LEFT_ARROW:
		go_to_coord(45,4);
	set_color_ground_console(desc_out,white,black);
	rus_text_printf_string("[����]");

	go_to_coord(33,4);
	set_color_ground_console(desc_out,white,green);
	rus_text_printf_string("[� ������]");
	flag = 2;
	break;

	}
	
	}
	else if(key == 13 && flag == 1)
	{
		set_color_ground_console(desc_out,white,black);
		system("CLS");
	go_to_coord(30,5);
	set_color_ground_console(desc_out,white,black);
	rus_text_printf_string("�� ����� ������ �� �������� ��� �� ������? ");

	go_to_coord(33,4);
	set_color_ground_console(desc_out,white,black);
	rus_text_printf_string("[�� ��������]");
	go_to_coord(50,4);
	rus_text_printf_string("[�� ������]");

	go_to_coord(33,4);
	set_color_ground_console(desc_out,white,green);
	rus_text_printf_string("[�� ��������]");

	flag = 3;
	while(1)
	{
	key = _getch();
	if(key == ARROWS)
	{
		key = _getch();
		switch(key)
	{
	case RIGTHT_ARROW:
		go_to_coord(33,4);
	set_color_ground_console(desc_out,white,black);
	rus_text_printf_string("[�� ��������]");

	go_to_coord(50,4);
	set_color_ground_console(desc_out,white,green);
	rus_text_printf_string("[�� ������]");
	flag = 4;
	break;
	case LEFT_ARROW:
		go_to_coord(50,4);
	set_color_ground_console(desc_out,white,black);
	rus_text_printf_string("[�� ������]");

	go_to_coord(33,4);
	set_color_ground_console(desc_out,white,green);
	rus_text_printf_string("[�� ��������]");
	flag = 3;
	break;

	}
	
	}
	else if(key == 13)
		return flag;
	}

	}
	else if (key == 13 && flag == 2)
		return flag;
	}

	
	return 0;

}

size_t UTIL_randomNum(size_t _iMin, size_t _iMax)
{
	return(_iMin + (rand()%(_iMax + 1 - _iMin)));
}