#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <time.h>
#define SIZE_FIELD 10

enum
{
	up = 1,
	right,
	left,
	down
};

int UTIL_randomNum(int _iMin, int _iMax);
int check_position(char field[SIZE_FIELD][SIZE_FIELD],int x_coord,int y_coord,int size_ship,int side);
void put_ship(char field[SIZE_FIELD][SIZE_FIELD],int y_coord,int x_coord,int size_ship,int side);
void filling_field(char field[SIZE_FIELD][SIZE_FIELD]);
void print_field(char field[SIZE_FIELD][SIZE_FIELD]);
int main()
{
	char field[SIZE_FIELD][SIZE_FIELD];
	memset(field,' ',SIZE_FIELD * SIZE_FIELD);
	srand((unsigned)time(NULL));

	filling_field(field);

	print_field(field);
	return 0;

}

void print_field(char field[SIZE_FIELD][SIZE_FIELD])
{
	int i,i1;

	system("CLS");

	for(i = 0;i < SIZE_FIELD;i++)
	{
		for(i1 = 0;i1 < SIZE_FIELD;i1++)
		{
			putchar(field[i][i1]);
		}
		printf("%d",i);
		putchar('\n');
	}
	for(i = 0;i < SIZE_FIELD;i++)
		printf("%d",i);
}

void filling_field(char field[SIZE_FIELD][SIZE_FIELD])
{
	int i;
	int ship = 1;
	int x_coord,y_coord,side;
	int flag = 0;

	while(ship < 5)
	{
		flag = 0;
		for(i = 0; i < ship;)
		{
			if(flag > 200)
			{
				memset(field,' ',SIZE_FIELD * SIZE_FIELD);
				flag = 0;
				ship = 0;
				break;
			}
			flag++;
			side = UTIL_randomNum(1,4);
			if(side == up)
			{
			x_coord = UTIL_randomNum(0,SIZE_FIELD-1);
			y_coord = UTIL_randomNum(((5-ship)+1),SIZE_FIELD-1);
			}
			else if(side == right)
			{
			x_coord = UTIL_randomNum(0,SIZE_FIELD-(ship+1));
			y_coord = UTIL_randomNum(0,SIZE_FIELD-1);
			}
			else if(side == down)
			{
			x_coord = UTIL_randomNum(0,SIZE_FIELD-1);
			y_coord = UTIL_randomNum(0,SIZE_FIELD-(ship+1));
			}
			else if(side == left)
			{
			x_coord = UTIL_randomNum(((5-ship)+1),SIZE_FIELD-1);
			y_coord = UTIL_randomNum(0,SIZE_FIELD-1);
			}

			if(check_position(field,x_coord,y_coord,5-ship,side))
			{
				put_ship(field,x_coord,y_coord,5-ship,side);
				i++;
			}
		}
		ship++;
	}
}
void put_ship(char field[SIZE_FIELD][SIZE_FIELD],int x_coord,int y_coord,int size_ship,int side)
{
	int i;

	for(i = 0;i< size_ship;i++)
	{
		field[y_coord][x_coord] = '#';
		
		if(side == up)  y_coord--;
		else if(side == right) x_coord++;
		else if(side == left) x_coord--;
		else if(side == down) y_coord++;
	}
}

int check_position(char field[SIZE_FIELD][SIZE_FIELD],int x_coord,int y_coord,int size_ship,int side)
{
	int i,y_now = y_coord,x_now = x_coord;
		y_now = y_coord;
		x_now = x_coord;

	for(i = 1;i <= 4;i++)
	{

		if(field[y_now][x_now] == '#' || y_now > SIZE_FIELD || y_now < 0 || x_now > SIZE_FIELD || x_now < 0) return 0; 
		else if(field[y_now][x_now+1] == '#' ) return 0; 
		else if(field[y_now][x_now-1] == '#' ) return 0; 
		else if(field[y_now+1][x_now] == '#' ) return 0; 
		else if(field[y_now-1][x_now] == '#' ) return 0; 
		else if(field[y_now+1][x_now+1] == '#') return 0; 
		else if(field[y_now+1][x_now-1] == '#') return 0; 
		else if(field[y_now-1][x_now+1] == '#') return 0; 
		else if(field[y_now-1][x_now-1] == '#') return 0; 
		
		if(side == up)  y_now--;
		else if(side == right) x_now++;
		else if(side == left) x_now--;
		else if(side == down) y_now++;
	}
	return 1;
}

int UTIL_randomNum(int _iMin, int _iMax)
{
	return(_iMin + (rand()%(_iMax + 1 - _iMin)));
}