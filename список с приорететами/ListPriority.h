#pragma once
#include<list>
#include<iostream>
#include<memory>


template<class type>
struct nodePriority
{
	nodePriority()
	{
		priority = 0;

	}
	nodePriority(const nodePriority& other)
	{
		m_data = other.m_data;
		m_priority = other.priority;
	}
	nodePriority(const type& data,size_t priority)
	{
		m_data = data;
		m_priority = priority;
	}
	nodePriority(const type&& data,size_t priority)
	{
		&m_data = std::move(data);
		m_priority = priority;
	}
	type m_data;
	size_t m_priority;
};

template <class type> 
class ListPriority
{
public:
	ListPriority(void)
	{
		countNodes = 0;
	}

	~ListPriority(void)
	{
		
		std::list<nodePriority<type>*>::iterator iter;
		for(iter = m_listPriority.begin() ; iter != m_listPriority.end() ; iter++)
		{
			delete (*iter);
		}

	}

	//void pushNewNode(type inNode,short priority = 0)
	//{
	//if(!m_listPriority.empty())
	//{
	//	std::list<std::unique_ptr<nodePriority<type>>>::iterator iter;
	//	for(iter = m_listPriority.begin() ; iter != m_listPriority.end() ; iter++)
	//	{
	//		if(m_listPriority.back() == m_listPriority.front()){ m_listPriority.push_back(std::move(inNode));break;}
	//		if(priority > (*iter)->priority)
	//		{
	//			std::unique_ptr<nodePriority<type>> new_ptr;
	//			new_ptr.reset(new nodePriority(inNode));
	//			m_listPriority.push_front(new_ptr);
	//			break;
	//		}
	//	}
	//}
	//else m_listPriority.push_back(std::move(inNode));
	//}

	void pushNewNode(type& inNode,size_t priority = 0)
	{
	nodePriority<type>* newNode = new nodePriority<type>(inNode,priority);
	if(!m_listPriority.empty())
	{
		bool done = false;
		std::list<nodePriority<type>*>::iterator iter;
		if(countNodes == 1)
			{
				if(priority > (*m_listPriority.back()).m_priority)
					m_listPriority.push_front(newNode);
				else
					m_listPriority.push_back(newNode);
			}
		else
		{
		for(iter = m_listPriority.begin() ; iter != m_listPriority.end() ; iter++)
		{
			if(priority > (*iter)->m_priority){m_listPriority.insert(iter,newNode);done = true;break;}
		}
		if(!done)
		{
			m_listPriority.push_back(newNode);
		}
		}
	}
	else m_listPriority.push_back(newNode);
	++countNodes;
	}

	void pushNewNode(type&& inNode,size_t priority = 0)
	{
		nodePriority<type>* newNode = new nodePriority<type>(inNode,priority);
	if(!m_listPriority.empty())
	{
		bool done = false;
		std::list<nodePriority<type>*>::iterator iter;
		if(countNodes == 1)
			{
				if(priority > (*m_listPriority.back()).m_priority)
					m_listPriority.push_front(newNode);
				else
					m_listPriority.push_back(newNode);
			}
		else
		{
		for(iter = m_listPriority.begin() ; iter != m_listPriority.end() ; iter++)
		{
			if(priority > (*iter)->m_priority){ m_listPriority.insert(iter,newNode);done = true;break;}
		}
		if(!done)
		{
			m_listPriority.push_back(newNode);
		}
		}
	}
	else m_listPriority.push_back(newNode);
	++countNodes;
	}

	//void pushNewNode(type* inNode,size_t priority = 0)
	//{
	//	nodePriority<type>* newNode = new nodePriority<type>(*inNode,priority);
	//if(!m_listPriority.empty())
	//{
	//	bool done = false;
	//	std::list<nodePriority<type>*>::iterator iter;
	//	if(countNodes == 1)
	//		{
	//			if(priority > (*m_listPriority.back()).m_priority)
	//				m_listPriority.push_front(newNode);
	//			else
	//				m_listPriority.push_back(newNode);
	//		}
	//	else
	//	{
	//	for(iter = m_listPriority.begin() ; iter != m_listPriority.end() ; iter++)
	//	{
	//		if(priority > (*iter)->m_priority){ m_listPriority.insert(iter,newNode);done = true;break;}
	//	}
	//	if(!done)
	//	{
	//		m_listPriority.push_back(newNode);
	//	}
	//	}
	//}
	//else m_listPriority.push_back(newNode);
	//++countNodes;
	//}

	type& Higest()
	{
		return m_listPriority.front()->m_data;
	}
	type& Lowest()
	{
		return m_listPriority.back()->m_data;
	}

	void popBack()
	{
		delete m_listPriority.back();
		m_listPriority.pop_back();
		--countNodes;
	}

	void popFront()
	{
		delete m_listPriority.front();
		m_listPriority.pop_front();
		--countNodes;
	}
	size_t size()
	{
		return countNodes;
	}

	std::list<type> getListPriority(size_t priority)
	{
		std::list<type> priorList;

		for(std::list<nodePriority<type>*>::iterator iter = m_listPriority.begin();iter != m_listPriority.end();iter++)
		{
			if((*iter)->m_priority == priority) priorList.push_back((*iter)->m_data);
		}

		return priorList;
	}
	//void iterBeg()
	//{
	//	if(!m_listPriority.empty())
	//	{
	//		iterator = m_listPriority.begin();
	//	}
	//}
	//void iterEnd()
	//{
	//	if(!m_listPriority.empty())
	//	{
	//		iterator = m_listPriority.end();
	//	}
	//}
	//type& iterGetNext()
	//{
	//	if(iterator != m_listPriority.end())
	//	{
	//		type& Return = (*iterator)->m_data;
	//		++iterator;
	//		return Return;
	//	}
	//	else return type();
	//}
	//type& iterGetPrev()
	//{
	//	if(iterator != m_listPriority.begin())
	//	{
	//		type& Return = (*iterator)->m_data;
	//		--iterator;
	//		return Return;
	//	}
	//	else return type();
	//}

	//static typedef m_listPriority.begin() iter_beg;
	//static typedef m_listPriority.end() iter_end;

private:
	std::list<nodePriority<type>*> m_listPriority; // ��������� ������  ���������� �� ��������� ��������� � ���������� ���� ������� �_�
	size_t countNodes;
	//std::list<nodePriority<type>*>::iterator iterator;
};

