#pragma once
#include<time.h>
#include<iostream>
class Worker
{
public:

	Worker(void)
	{
		beginWorkTime = 0;
		endWorkTime = 0;
		m_status = rest;
	}
	bool status()
	{
		return static_cast<bool>(m_status);
	}
	void setWork(clock_t inTime)
	{
		beginWorkTime =	clock();
		endWorkTime = inTime + beginWorkTime;
		m_status = busy;
	}
	bool checkBusy()
	{
		if(m_status == busy)
		if(clock() > endWorkTime)
		{
		m_status = rest;
		beginWorkTime = 0;
		endWorkTime = 0;
		return true;
		}
		return false;
	}


private:
	unsigned long long beginWorkTime;
	unsigned long long endWorkTime;
	enum WorkStatus
	{
		busy,
		rest
	} m_status;

};

