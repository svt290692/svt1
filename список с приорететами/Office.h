#pragma once
#include"Worker.h"
#include"Client.h"
#include"ListPriority.h"
#include<list>
#include<time.h>
#include<vector>
class Office
{
public:
	Office(void);
	Office(size_t m_workers);
	~Office(void);

	void step();
	void printStatus()const;

private:
	std::vector<Worker> m_workers;
	size_t countWorkers;
	size_t workingNow;
	ListPriority<Client> Clients;
	size_t waitClient;
};

