#include <stdio.h>
#include <stdlib.h>
#include <string>

typedef struct sign_node
{
	char sign;
	int  count;
	char ciper[64];
	struct sign_node * left_node;
	struct sign_node * right_node;

} SYM,*NODE_POINT;

extern FILE *file_write;
extern FILE *file_decoder;
extern FILE *file_exit;
extern FILE *file_temp;
extern FILE *file_temp1;
extern unsigned long long last_sign;
extern int count_regulator;
extern char *list_numbers;

void create_node_tree(NODE_POINT *root,char sign); // ������� ���������� �� �������
void create_signs_tree(NODE_POINT *root,FILE *file);// ������� �������� ������ �������� ��������� �� ����� � �� ��������
void create_counts_tree(NODE_POINT *root,char sign,int count,NODE_POINT left,NODE_POINT right);//������ ���������� ��� �������� �������� �� ��������� � �� �� ��������
void nulling_tree(NODE_POINT *root);// ������� ��������� ������ ������
void rebild_tree(NODE_POINT *root,NODE_POINT *new_root);// ������� ������������ ������
NODE_POINT destribution(NODE_POINT *root); // ������� ������������� ���������� �� ����� � ������ ������
void filling_massive(NODE_POINT *massive_to_filling,NODE_POINT root_of_tree);
void print_massive_node(NODE_POINT *massive); // ������ ������������ �������
int strlen_SYM(NODE_POINT *massive);
void sort_massive(NODE_POINT *massive,int size_mas);
void del_elm_mas(NODE_POINT *massive,int elm);
NODE_POINT create_haffman_node(NODE_POINT left,NODE_POINT right);
NODE_POINT building_haffman(NODE_POINT *massive);
void create_ciper(NODE_POINT root,char *sign);
void print_haf(NODE_POINT root);
NODE_POINT seek_sign(NODE_POINT root,char sign);
int create_haffman_output(NODE_POINT root, FILE *file);
void decoder(NODE_POINT root,int teil,FILE *file);
void file_decomp_ini(FILE *file_in,FILE *file_out);